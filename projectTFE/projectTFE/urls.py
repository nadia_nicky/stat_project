"""projectTFE URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from drf_yasg2.views import get_schema_view
from drf_yasg2 import openapi

from myapp import api
from rest_framework import routers, permissions

# API REGISTRATION
rooter = routers.DefaultRouter()
rooter.register('bme280data-api', api.BME280DATAViewSet, basename='base-api-bme280data')
rooter.register('account-api', api.AccountViewSet, basename='base-api-account')
rooter.register('mq7-api', api.MQ7ViewSet, basename='base-api-mq7')

# API DOCS CONFIG
schema_view = get_schema_view(
    openapi.Info(
        title="API Project TFE",
        default_version='v1',
        description="Documentation de l'API",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="admin@site.com"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('myapp.urls')),
    path("api/", include(rooter.urls)),

    path('swagger(<format>\.json|\.yaml)', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('apidoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
