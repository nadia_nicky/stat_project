from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import *

# Register your models here.


class BME280DATAAdmin(admin.ModelAdmin):
    list_display = ["temperature", "pressure", "humidity", "date_add"]
    list_filter = [
        "date_add",
    ]

class MQ7Admin(admin.ModelAdmin):
    list_display = ["voltage", "value", "warning", "date_add"]
    list_filter = [
        "warning",
        "date_add",
    ]

    date_hierarchy = "date_add"

class AccountAdmin(UserAdmin):
    model = Account
    list_display = (
        "username",
        "last_name",
        "city",
        "first_name",
        "email",
        "is_active",
        "is_staff",
        "is_superuser",
    )
    list_display_links = (
        "username",
        "last_name",
        "first_name",
        "email",
    )

    fieldsets = UserAdmin.fieldsets + ((None, {"fields": ("city",)}),)

    add_fieldsets = UserAdmin.add_fieldsets + ((None, {"fields": ("city",)}),)

    list_filter = UserAdmin.list_filter

    search_fields = (
        "email",
        "last_name",
    )
    ordering = ("email",)


admin.site.register(Account, AccountAdmin)
admin.site.register(BME280DATA, BME280DATAAdmin)
admin.site.register(MQ7, MQ7Admin)