from django.urls import path, include
from .views import account, dashboard, getbme280,mq7

urlpatterns = [
    # account
    path("login", account.connection, name="connection"),
    path("register", account.register, name="register"),
    path("logout", account.logout_view, name="logout"),
    path("cgu", account.cgu, name="cgu"),
    path("api/login/", account.login_view, name="login"),
    path("api/register/", account.inscription, name="inscription"),
    # dashboard
    path("", dashboard.home, name="home"),
    path("temperature", dashboard.temperature, name="temperature"),
    path("humidite", dashboard.humidite, name="humidite"),
    path("pression", dashboard.pression, name="pression"),
    path("gaz", dashboard.gaz, name="gaz"),
    path("evolution", dashboard.evolution, name="evolution"),
    path("profile", dashboard.profile, name="profile"),
    # API connect Raspberry
    path("api/bme280/", getbme280.getbme280data, name="databme280"),
    path("api/mq7/", mq7.getmq7data, name="databmemq7"),
    # API graph data
    path("api/getmq7/", mq7.apimq7data, name="apimq7data"),
    # New root
    path("users", dashboard.utilisateur, name="users"),
]