from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from myapp.models import Account
from django.db.models import Q
from django.urls import reverse
from random_username.generate import generate_username

# Create your views here.


def connection(request):

    return render(request, "pages/accounts/login.html", locals())


def register(request):

    return render(request, "pages/accounts/register.html", locals())


def cgu(request):

    return render(request, "pages/accounts/cgu.html", locals())


def profile(request):

    return render(request, "pages/accounts/profile.html", locals())


# API Login
@csrf_exempt
def login_view(request):
    if request.method == "POST":
        message, success, url = "", False, ""
        email = request.POST.get("email")
        password = request.POST.get("password")
        user = Account.objects.filter(
            Q(email=email) | Q(username=email) & Q(is_active=True)
        ).first()
        next = request.POST.get("next")

        if user:
            utilisateur = authenticate(username=user.username, password=password)
            if utilisateur:
                login(request, utilisateur)
                message, success = "Connexion effectué", True
                if next != "None":
                    url = next
                else:
                    url = reverse("home", kwargs={})
                print(url)
            else:
                message = "Vos identifiants ne sont pas correctes."
        else:
            message = "Vos identifiants n'existent pas."
        datas = {
            "message": message,
            "success": success,
            "url": url,
        }
        return JsonResponse(datas, safe=False)


# API Inscription
@csrf_exempt
def inscription(request):
    if request.method == "POST":
        message, success, url = "", False, ""
        email = request.POST.get("email")
        password = request.POST.get("password")
        lastname = request.POST.get("lastname")
        firstname = request.POST.get("firstname")
        city = request.POST.get("city")
        username = generate_username(1)[0]
        try:
            user = Account(
                email=email,
                first_name=firstname,
                last_name=lastname,
                city=city,
                username=username,
            )
            user.save()
            user.set_password(password)
            user.save()
            if user:
                utilisateur = authenticate(username=user.username, password=password)
                if utilisateur:
                    login(request, utilisateur)
                    message, success = "Inscription effectué avec succès", True
                else:
                    message = "Vos identifiants ne sont pas correctes."
        except Exception as e:
            print(str(e))
            message = "Un compte existe déjà avec cet email."
        datas = {
            "message": message,
            "success": success,
            "url": url,
        }
        return JsonResponse(datas, safe=False)


def logout_view(request):
    logout(request)
    return redirect("connection")