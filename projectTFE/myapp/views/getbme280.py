from django.shortcuts import render
from django.http import JsonResponse, request
from django.views.decorators.csrf import csrf_exempt
from myapp.models import BME280DATA


@csrf_exempt
def getbme280data(request):
    timestamp = request.POST.get("timestamp")
    temperature = request.POST.get("temperature")
    pressure = request.POST.get("pressure")
    humidity = request.POST.get("humidity")
    print(temperature, pressure, humidity, timestamp)
    bme = BME280DATA(
        date_add=timestamp,
        temperature=temperature,
        pressure=pressure,
        humidity=humidity
    )
    bme.save()
    data = {}
    return JsonResponse(data, safe=False)
