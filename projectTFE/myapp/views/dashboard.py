from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
import requests
from django.db.models import Count, Avg
from django.db.models.functions import TruncDay
from myapp import models


# Create your views here.


@login_required(login_url="connection")
def utilisateur(request):
    if request.user.is_superuser:
        users = models.Account.objects.exclude(username=request.user.username)
        return render(request, "pages/dashboard/utilisateur.html", locals())
    else:
        return redirect("home")


@login_required(login_url="connection")
def home(request):
    # your API KEY
    API_KEY = "9c5213ce52fe7ffa15a904fc80a5f879"
    # User city
    city = request.user.city  # "bruxelles"
    # number of day forecast
    cnt = 7
    # API root
    url = f"https://api.openweathermap.org/data/2.5/forecast/daily?q={city}&units=metric&appid={API_KEY}&cnt={cnt}"
    # url = "https://api.openweathermap.org/data/2.5/onecall?lat=5.31&lon=-4.03&exclude=minutely,hourly&units=metric&appid=c390711dcd13b8757f36ef76665e45e3"

    # Response
    req = requests.get(url)
    data = req.json()
    page = "Home"
    # ----- get raspberry current data
    bme280 = models.BME280DATA.objects.order_by("-date_add").first()
    mq7 = models.MQ7.objects.order_by("-date_add").first()

    return render(request, "pages/dashboard/index.html", locals())


@login_required(login_url="connection")
def temperature(request):
    page = "Temperature"
    # ----- get raspberry current data
    bme280 = models.BME280DATA.objects.order_by("-date_add").first()
    bme280_24 = models.BME280DATA.objects.order_by("-date_add")[:24]
    weekbme280 = (
        models.BME280DATA.objects.annotate(day=TruncDay("date_add"))
            .values("day")
            .annotate(
            cnt=Count("id"),
            temp=Avg("temperature"),
        )
            .values("day", "cnt", "temp")
            .order_by("-day")[:7]
    )
    return render(request, "pages/dashboard/temperature.html", locals())


@login_required(login_url="connection")
def humidite(request):
    page = "Humidite"
    # ----- get raspberry current data
    bme280 = models.BME280DATA.objects.order_by("-date_add").first()
    bme280_24 = models.BME280DATA.objects.order_by("-date_add")[:24]
    weekbme280 = (
        models.BME280DATA.objects.annotate(day=TruncDay("date_add"))
            .values("day")
            .annotate(
            cnt=Count("id"),
            temp=Avg("humidity"),
        )
            .values("day", "cnt", "temp")
            .order_by("-day")[:7]
    )
    return render(request, "pages/dashboard/humidite.html", locals())


@login_required(login_url="connection")
def pression(request):
    page = "Pression"
    # ----- get raspberry current data
    bme280 = models.BME280DATA.objects.order_by("-date_add").first()
    bme280_24 = models.BME280DATA.objects.order_by("-date_add")[:24]
    weekbme280 = (
        models.BME280DATA.objects.annotate(day=TruncDay("date_add"))
            .values("day")
            .annotate(
            cnt=Count("id"),
            temp=Avg("pressure"),
        )
            .values("day", "cnt", "temp")
            .order_by("-day")[:7]
    )
    return render(request, "pages/dashboard/pression.html", locals())


def gaz(request):
    page = "Gaz"
    gaz = models.MQ7.objects.order_by("-date_add").first()
    gazs = models.MQ7.objects.order_by("-date_add")[:10]
    weekgaz = (
        models.MQ7.objects.annotate(day=TruncDay("date_add"))
            .values("day")
            .annotate(
            cnt=Count("id"),
            val=Avg("value"),
            volt=Avg("voltage"),
        )
            .values("day", "cnt", "val", "volt")
            .order_by("-day")[:3]
    )

    print(weekgaz)
    return render(request, "pages/dashboard/gaz.html", locals())


def evolution(request):
    page = "Evolution"
    return render(request, "pages/dashboard/evolution.html", locals())


def profile(request):
    page = "Apropos"
    return render(request, "pages/dashboard/profile.html", locals())
