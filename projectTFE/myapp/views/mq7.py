from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from myapp import models
from django.utils.timezone import datetime
from django.db.models.functions import TruncDay
from django.db.models import Count, Avg
from datetime import timedelta
from django.core.mail import send_mail


@csrf_exempt
def getmq7data(request):
    if request.method == "POST":
        # - get data from raspberry MQ7
        voltage = request.POST.get("voltage")
        ppm = request.POST.get("value")
        warning = request.POST.get("warning")
        # -save data in database
        gaz = models.MQ7(
            voltage=voltage,
            value=ppm,
            warning=warning,
            date_add=datetime.now() + timedelta(hours=2)
        )
        gaz.save()
        emails = []
        if gaz.warning == "True":

            for user in models.Account.objects.all():
                emails.append(user.email)
            send_mail(
                subject="Warning",
                message="Attention  detection de monoxyde de carbone dans votre maison ",
                from_email="tfet93327@gmail.com",
                recipient_list=emails,
                auth_user="tfet93327@gmail.com",
                auth_password="Mamoute1",
                fail_silently=False,
            )

        print("Save", voltage, ppm)

    data = {}
    return JsonResponse(data, safe=False)


@csrf_exempt
def apimq7data(request):
    gazs = models.MQ7.objects.values().order_by("-date_add")[:20]
    weekgaz = (
        models.MQ7.objects.annotate(day=TruncDay("date_add"))
            .values("day")
            .annotate(
            cnt=Count("id"),
            val=Avg("value"),
            volt=Avg("voltage"),
        )
            .values("day", "cnt", "val", "volt")
            .order_by("day")[:10]
    )
    data = list(gazs)
    weekdata = list(weekgaz)
    donne = []
    for item in data:
        if item["warning"]:
            d = {"value": item["value"], "itemStyle": {"color": "#a90000"}}
        else:
            d = item["value"]
        donne.append(d)

    option = {
        "tooltip": {
            "trigger": "axis",
            "axisPointer": {"type": "cross", "crossStyle": {"color": "#999"}},
        },
        "legend": {"data": ["ppm", "Volatage"]},
        "xAxis": [
            {
                "type": "category",
                "data": list(reversed([item["date_add"].time() for item in data])),
                "axisPointer": {"type": "shadow"},
            }
        ],
        "yAxis": [
            {"type": "value", "name": "Ppm", "axisLabel": {"formatter": "{value} ppm"}},
            {
                "type": "value",
                "name": "Volatage",
                "axisLabel": {"formatter": "{value} V"},
            },
        ],
        "series": [
            {
                "name": "ppm",
                "type": "bar",
                "data": list(reversed(donne)),
            },
            {
                "name": "Volatage",
                "type": "line",
                "yAxisIndex": 1,
                "data": list(reversed([item["voltage"] for item in data])),
            },
        ],
    }

    weekoption = {
        "tooltip": {
            "trigger": "axis",
            "axisPointer": {"type": "cross", "crossStyle": {"color": "#999"}},
        },
        "legend": {"data": ["ppm", "Volatage"]},
        "xAxis": [
            {
                "type": "category",
                "data": [item["day"].date() for item in weekdata],
                "axisPointer": {"type": "shadow"},
            }
        ],
        "yAxis": [
            {"type": "value", "name": "Ppm", "axisLabel": {"formatter": "{value} ppm"}},
            {
                "type": "value",
                "name": "Volatage",
                "axisLabel": {"formatter": "{value} V"},
            },
        ],
        "series": [
            {
                "name": "ppm",
                "type": "bar",
                "data": [item["val"] for item in weekdata],
            },
            {
                "name": "Volatage",
                "type": "line",
                "yAxisIndex": 1,
                "data": [item["volt"] for item in weekdata],
            },
        ],
    }
    laste_data = data[0]
    datas = {
        "option": option,
        "weekoption": weekoption,
        "laste_data": laste_data,
    }

    return JsonResponse(datas, safe=False)
