from rest_framework import serializers
from . import models


class BME280DATASerializer(serializers.ModelSerializer):
    class Meta:
        model = models.BME280DATA
        fields = '__all__'


class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Account
        fields = (
            'city',
            'username',
            'first_name',
            'last_name',
            'email',
            'password',
            'is_superuser'
        )
        extra_kwargs = {
            'password': {'write_only': True}
        }


class MQ7Serializer(serializers.ModelSerializer):
    class Meta:
        model = models.MQ7
        fields = '__all__'
