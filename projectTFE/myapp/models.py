from django.db import models
from django.contrib.auth.models import AbstractUser


# Create your models here.
class BME280DATA(models.Model):
    """Model definition for BME280."""

    # TODO: Define fields here
    date_add = models.DateTimeField()
    temperature = models.DecimalField(max_digits=20, decimal_places=2)
    pressure = models.DecimalField(max_digits=20, decimal_places=2)
    humidity = models.DecimalField(max_digits=20, decimal_places=2)

    class Meta:
        """Meta definition for BME280."""

        verbose_name = "BME280 DATA"
        verbose_name_plural = "BME280 DATA"

    def __str__(self):
        """Unicode representation of BME280."""
        return f"{self.temperature} °C, {self.pressure} hPa, {self.humidity} % rH"


class Account(AbstractUser):
    city = models.CharField(verbose_name="City", max_length=100)

    def __str__(self):
        return self.email


class MQ7(models.Model):
    """Model definition for MQ7."""

    # TODO: Define fields here
    voltage = models.DecimalField(max_digits=20, decimal_places=2)
    value = models.DecimalField(max_digits=20, decimal_places=2)
    date_add = models.DateTimeField()
    warning = models.BooleanField(default=False)

    class Meta:
        """Meta definition for MQ7."""

        verbose_name = "MQ7"
        verbose_name_plural = "MQ7s"

    def __str__(self):
        """Unicode representation of MQ7."""
        return f"{self.voltage} V - {self.value} ppm"
