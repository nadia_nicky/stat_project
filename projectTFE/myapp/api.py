from rest_framework import viewsets
from . import serializers
from . import models


class BME280DATAViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.BME280DATASerializer
    queryset = models.BME280DATA.objects.all()


class AccountViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.AccountSerializer
    queryset = models.Account.objects.all()


class MQ7ViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.MQ7Serializer
    queryset = models.MQ7.objects.all()