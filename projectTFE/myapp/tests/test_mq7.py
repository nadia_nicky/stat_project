from myapp.tests import TestCase
from myapp.models import MQ7b


class MQ7TestCase(TestCase):

    # test get all mq7 data
    def test_get_all_mq7(self):
        mq7 = MQ7.objects.all()
        self.assertEqual(mq7.count(), 4)

    # test add mq7 data
    def test_add_mq7_by_api(self):
        data = {
            "voltage": "1.2",
            "value": "2000",
            "date_add": "2021-07-29T17:41:28+00:00",
            "warning": "False",
        }*9

        response = self.client.post("/api/mq7/", data)
        self.assertEqual(response.status_code, 200)

        mq7 = MQ7.objects.all()
        self.assertEqual(mq7.count(), 5)
