from myapp.tests import TestCase
from myapp.models import BME280DATA


class BM280TestCase(TestCase):

    # MODELS: test get all bm280 data
    def test_get_all_bme280(self):
        bm280 = BME280DATA.objects.all()
        self.assertEqual(bm280.count(), 3)

    # API: test add bm280 data
    def test_add_bme280_by_api(self):
        data = {
            "timestamp": "2021-07-29T17:41:28+00:00",
            "temperature": "17",
            "pressure": "112",
            "humidity": "65",
        }

        response = self.client.post("/api/bme280/", data)
        self.assertEqual(response.status_code, 200)

        bm280 = BME280DATA.objects.all()
        self.assertEqual(bm280.count(), 4)
