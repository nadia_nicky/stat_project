import schedule
import time, requests

import smbus2
import bme280


def getDataByBME280():
    port = 1
    address = 0x76
    bus = smbus2.SMBus(port)

    calibration_params = bme280.load_calibration_params(bus, address)

    # # the sample method will take a single reading and return a
    # # compensated_reading object
    data = bme280.sample(bus, address, calibration_params)

    # the compensated_reading class has the following attributes
    content = {}

    content["timestamp"] = data.timestamp
    content["temperature"] = data.temperature
    content["pressure"] = data.pressure
    content["humidity"] = data.humidity

    return content


def job():
    
    data = getDataByBME280()  # data to post
    url = "https://station-tfe.herokuapp.com/api/bme280/"
    #url = "https://station-tfe.herokuapp.com/api/bme280/"  # django project post data root
    req = requests.post(url=url, data=data)  # send data to django project
    print(req.status_code)

    print("I'm working...")


# run job every 10 seconds
schedule.every(1).hour.do(job)
# run job every 10 minutes
# schedule.every(10).minutes.do(job)
#schedule.every(10).seconds.do(job)

while True:
    schedule.run_pending()
    time.sleep(1)
