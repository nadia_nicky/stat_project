import schedule
import time, requests


from time import sleep
import board
import busio
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn
import RPi.GPIO as GPIO


def getDataByBMQ7():
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(17, GPIO.IN)
    # Create the I2C bus
    i2c = busio.I2C(board.SCL, board.SDA)
    # Create the ADC object using the I2C bus
    ads = ADS.ADS1115(i2c)
    ads.gain = 2 / 3
    # Create single-ended input on channels
    chan0 = AnalogIn(ads, ADS.P0)
    chan1 = AnalogIn(ads, ADS.P1)
    chan2 = AnalogIn(ads, ADS.P2)
    chan3 = AnalogIn(ads, ADS.P3)
    # print("{:>5.3f}".format(chan0.voltage))
    # print( dir(chan0))

    warning = False
    value = chan0.value
    voltage = chan0.voltage
    if GPIO.input(17) == GPIO.LOW:
        print("Warning: Limit exceeded!!!")
        print("Warning value: ", chan0.value)
        print(chan0.voltage)
        warning = True
    print(chan0.value)
    print("{:>5.3f}".format(chan0.voltage))

    # the compensated_reading class has the following attributes
    content = {}

    content["warning"] = warning
    content["value"] = value
    content["voltage"] = voltage

    return content


def job():

    data = getDataByBMQ7()  # data to post
    url = "https://station-tfe.herokuapp.com/api/mq7/"# django project post data root
    
    #url = "http://192.168.0.199:8000/api/mq7/"
    req = requests.post(url=url, data=data)  # send data to django project
    print(req.status_code)

    print("I'm working...")


# run job every 10 seconds
schedule.every(5).seconds.do(job)
# run job every 10 minutes
# schedule.every(10).minutes.do(job)


while True:
    schedule.run_pending()
    time.sleep(1)